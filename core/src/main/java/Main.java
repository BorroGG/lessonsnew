import model.Human;
import util.HumanUtil;

import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.money.convert.CurrencyConversion;
import javax.money.convert.MonetaryConversions;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    public static void main(String[] args) {
        Human petr = new Human(
                "Petr",
                "Вetrov",
                "Petrovich",
                20,
                "M",
                "20.06.2000");
        Human vanya = new Human(
                "Vanya",
                "дvanov",
                "Ivanovich",
                18, "M",
                "01.04.2002");
        Human max = new Human(
                "Max",
                "Maximov",
                "MAximovich",
                50,
                "M",
                "10.03.1970");
        Human ilya = new Human(
                "Ilya",
                "Ilinov",
                "Ilin",
                25,
                "M",
                "18.05.1995");
        List<Human> humans = new LinkedList<>();
        humans.add(petr);
        humans.add(vanya);
        humans.add(vanya);
        humans.add(max);
        humans.add(max);
        humans.add(ilya);
        humans.add(ilya);
        //Task 1.1
        printContainsTemplate(humans);
        //Task 1.2
        printListWihFilter(humans);
        //Task 1.3
        printDuplicatesInList(humans);
        //Task 2
        System.setErr(new PrintStream(new OutputStream() {
            public void write(int b) {
            }
        }));
        convertMoney(1000, "RUB", "USD");
        convertMoney(12.96, "USD", "RUB");
    }

    private static void printContainsTemplate(List<Human> humans) {
        System.out.println("Contains? : " +
                humans.contains(HumanUtil
                        .getNewHuman("Petr", "Petrov")));
    }

    private static void printListWihFilter(List<Human> humans) {
        humans.stream().filter(human -> human.getAge() < 21 &&
                (human.getSurname().toLowerCase().matches("[абвгд].*")))
                .forEach(System.out::println);
    }

    private static void printDuplicatesInList(List<Human> humans) {
        Set<Human> humanSet = new HashSet<>();
        AtomicInteger x = new AtomicInteger();
        humans.stream()
                .filter((s) -> {
                    if (humanSet.contains(s)) return true;
                    humanSet.add(s);
                    return false;
                })
                .sorted((Comparator.comparing(Human::getAge).reversed()))
                .forEach((e) -> System.out.println("{" + x.incrementAndGet() + ":" + e + "}"));
    }

    private static void convertMoney(double countMoney, String fromCurrency, String toCurrency) {
        System.out.println("Loading...");
        MonetaryAmount money = Monetary.getDefaultAmountFactory().setCurrency(fromCurrency)
                .setNumber(countMoney).create();

        CurrencyConversion conversion = MonetaryConversions.getConversion(toCurrency);

        MonetaryAmount convertedAmount = money.with(conversion);

        System.out.println(convertedAmount);
    }

}
